#!/usr/bin/python
import re

f = open("generate_maze.dump", "r")
l = f.readline()

while l:
	# find <main>:
	if(l.find("<main>:") != -1):
		break
	l = f.readline()
head = re.search(r'(\w+)', l).group()
	
while l:
	# find ret:
	if(l.find("ret") != -1):
		break
	l = f.readline()
tail = re.search(r'(\w+)', l).group()

f.close()

f = open("generate_maze.log", "r")

l1 = f.readline()
l2 = f.readline()

while l1:
	# meet main:
	if(l1.find(head) != -1):
		break
	l1 = f.readline()
	l2 = f.readline()
while l1:
	# meet ret:
	if(l1.find(tail) != -1):
		break
	# load instruction
	instruction_mem = re.search(r'0x(\w+)', l1).group()
	print("l",instruction_mem)
	
	# check instruction
	if(l2.find("mem") != -1):
		if(l1.find("ld") != -1):
			load_mem = re.search(r'mem 0x(\w+)', l2).group()
			load_mem = load_mem[4::1]
			print("l",load_mem)
		if(l1.find("lb") != -1):
			load_mem = re.search(r'mem 0x(\w+)', l2).group()
			load_mem = load_mem[4::1]
			print("l",load_mem)
		if(l1.find("lw") != -1):
			load_mem = re.search(r'mem 0x(\w+)', l2).group()
			load_mem = load_mem[4::1]
			print("l",load_mem)
		if(l1.find("sd") != -1):
			load_mem = re.search(r'mem 0x(\w+)', l2).group()
			load_mem = load_mem[4::1]
			print("s",load_mem)
		if(l1.find("sb") != -1):
			load_mem = re.search(r'mem 0x(\w+)', l2).group()
			load_mem = load_mem[4::1]
			print("s",load_mem)
		if(l1.find("sw") != -1):
			load_mem = re.search(r'mem 0x(\w+)', l2).group()
			load_mem = load_mem[4::1]
			print("s",load_mem)
	
	l1 = f.readline()
	l2 = f.readline()

f.close()