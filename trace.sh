#!/bin/bash

#/opt/riscv is install path of your riscv tool
export PATH=/opt/riscv/bin:$PATH

read -p "Please input hegiht of maze: " hegiht
read -p "Please input width of maze:  " width
read -p "Please input row number of start cell:  " i
read -p "Please input column number of start cell:  " j

# write input file
echo -e "${hegiht}\n${width}\n${i}\n${j}\n\n" > in

riscv64-unknown-elf-gcc -o generate_maze main.c generate_maze.c lfsr.c

spike -l --log-commits pk generate_maze < in 2> generate_maze.log

riscv64-unknown-elf-objdump -d generate_maze > generate_maze.dump

python3 trace.py > maze.trace
